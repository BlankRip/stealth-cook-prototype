﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class B_MoveTest : MonoBehaviour
{
    [SerializeField] float speed = 2;
    private CharacterController cc;
    private Vector3 moveDir;

    private void Start() {
        cc = GetComponent<CharacterController>();
    }

    private void OnMove(InputValue value){ 
        Vector2 moveValue = value.Get<Vector2>();
        moveDir = new Vector3(moveValue.x, 0, moveValue.y);
    }

    private void Update() {
        cc.Move(moveDir * speed * Time.deltaTime);
    }
}
